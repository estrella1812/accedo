# Accedo Test

Project generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Executing local server

1) Clone project
2) Must install: node and angular-cli
3) Executing installation of the project: `npm install`
4) Executing local server: `ng serve`
Access: `http://localhost:4200/`

## Publishing

Execute command: `ng build`
Publishing files generated in folder: `dist/`
