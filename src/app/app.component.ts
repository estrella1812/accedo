import { Component, OnInit } from '@angular/core';
import { LoaderService } from './services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.sass']
})

export class AppComponent implements OnInit {
    public title = 'Accedo';
    public showLoader: boolean;

    constructor(
        private loaderService: LoaderService) {}

    public ngOnInit() {
        this.loaderService.status.subscribe((val: boolean) => {
            this.showLoader = val;
        });
    }
}
