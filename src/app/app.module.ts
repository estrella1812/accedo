import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routing';
import { PagesModule } from './pages/pages.module';
import { PipesModule } from './pipes/pipes.module';

import { LoaderService } from './services/loader.service';
import { HttpService } from './services/http.service';
import { ComponentsModule } from './components/components.module';
import { NguCarouselModule } from '@ngu/carousel';

@NgModule({
    declarations: [
        AppComponent
    ],

    imports: [
        BrowserModule,
        RouterModule,
        AppRoutingModule,
        PagesModule,
        PipesModule,
        ComponentsModule,
        NguCarouselModule
    ],

    providers: [
        HttpService,
        LoaderService
    ],

    bootstrap: [
        AppComponent
    ]
})

export class AppModule {}
