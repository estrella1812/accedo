import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderSiteComponent } from './header-site/header-site.component';
import { RouterModule } from '@angular/router';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { ModalHistoryComponent } from './modal-history/modal-history.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule
    ],
    declarations: [
        HeaderSiteComponent,
        VideoPlayerComponent,
        ModalHistoryComponent
    ],
    exports: [
        HeaderSiteComponent,
        VideoPlayerComponent,
        ModalHistoryComponent
    ]
})

export class ComponentsModule {}
