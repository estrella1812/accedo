import { Component, OnInit, Input, Output } from '@angular/core';
import { VideosUser } from '../../models/videos-user';

@Component({
    selector: 'app-header-site',
    templateUrl: './header-site.component.html'
})

export class HeaderSiteComponent implements OnInit {
    public openedHistory: boolean;
    public history: VideosUser;
    @Input('title') title: string;

    public ngOnInit() {
    }

    public openHistory() {
        this.history = JSON.parse(sessionStorage.getItem('videosUser'));
        this.openedHistory = true;
    }

    public closeHistory(data) {
        this.openedHistory = false;
    }

}
