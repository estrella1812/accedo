import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VideosUser } from '../../models/videos-user';

@Component({
    selector: 'app-modal-history',
    templateUrl: './modal-history.component.html',
    styleUrls: ['./modal-history.component.sass']
})
export class ModalHistoryComponent implements OnInit {
    @Input('history') history: Array<VideosUser>;
    @Output() close = new EventEmitter;

    constructor() { }

    ngOnInit() {
    }

    public closeHistory() {
        this.close.emit('close');
    }

}
