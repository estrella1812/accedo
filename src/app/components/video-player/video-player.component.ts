import { Component, OnInit, Output, Input, EventEmitter, HostListener } from '@angular/core';
import { Movie, Content } from '../../models/movies';

@Component({
    selector: 'app-video-player',
    templateUrl: './video-player.component.html',
    styleUrls: ['./video-player.component.sass']
})
export class VideoPlayerComponent implements OnInit {
    @Input('movie') movie: Movie;
    @Input('playback') playback: number;
    @Output() infoVideo = new EventEmitter();
    public video: HTMLVideoElement;
    @HostListener('document:keydown', ['$event']) _handleKeyboardEvent(event: KeyboardEvent) {
        if (event.key === 'Escape') {
            this.closeVideo();
        }
    }

    constructor() { }

    ngOnInit() {
        this.video = document.getElementById('videoPlayer') as HTMLVideoElement;
        this.video.currentTime = this.playback;
        this.video.onended = () => {
            this.infoVideo.emit({
                id: this.movie.id,
                stoppedAt: 0,
                title: this.movie.title,
                images: this.movie.images[0]
            });
        };
        this.video.focus();
    }

    public closeVideo() {
        this.infoVideo.emit({
            id: this.movie.id,
            stoppedAt: this.video.currentTime,
            title: this.movie.title,
            images: this.movie.images[0]
        });
    }

}
