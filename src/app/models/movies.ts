export class Catalog {
    public totalCount: number;
    public entries: Array<Movie>;
}

export class Movie {
    public title: string;
    public description: string;
    public contents: Array<Content>;
    public id: string;
    public images: Array<Image>;
}

export class Content {
    public url: string;
    public format: string;
    public width: number;
    public height: number;
    public language: string;
    public duration: string;
    public id: string;
}

export class Image {
    public type: string;
    public url: string;
    public width: number;
    public height: number;
    public id: string;
}
