export class VideosUser {
    public id: string;
    public stoppedAt: number;
    public title: string;
    public images: any;
}
