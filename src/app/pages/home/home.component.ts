import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { element } from 'protractor';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../models/movies';
import { VideosUser } from '../../models/videos-user';
import { NguCarouselService  } from '@ngu/carousel';

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.sass']
})

export class HomeComponent implements OnInit {
    public movies: Array<Movie>;
    public totalCount: number;
    public movie: Movie;
    public playback: number;
    public carouselConfig = {
        grid: {xs: 2, sm: 3, md: 3, lg: 4, all: 0},
        slide: 2,
        speed: 400,
        animation: 'lazy',
        point: {
            visible: false
        },
        load: 2,
        easing: 'ease'
    };
    @HostListener('document:keydown', ['$event']) keyboardEvent = this._handleKeyboardEvent;
    @HostListener('document:mousedown', ['$event']) mouseEvent = this._handleMouseEvent;
    @ViewChild('myCarousel') carousel: NguCarouselService;

    constructor(
        private moviesService: MoviesService
    ) {}

    public async ngOnInit() {
        const videosUser = sessionStorage.getItem('videosUser');

        if (!videosUser) {
            const videos = [];
            sessionStorage.setItem('videosUser', JSON.stringify(videos));
        }

        this.loadApi();
    }

    public async loadApi() {
        try {
            const response = await this.moviesService.getMovies();
            this.movies = response.entries;
            this.totalCount = response.totalCount;
            setTimeout(() => {
                const linhas = document.querySelectorAll('.line');
                if (linhas.length > 0) {
                    linhas[0].classList.add('selected');
                }
            }, 0);
        } catch (e) {
            console.error(e);
        }
    }

    public openMovie(movie) {
        const videosUser: Array<VideosUser> = JSON.parse(sessionStorage.getItem('videosUser'));
        const foundVideo = videosUser.findIndex((obj) => obj.id === movie.id);

        if (foundVideo === -1) {
            this.playback = 0;
        } else {
            this.playback = videosUser[foundVideo].stoppedAt;
        }

        this.movie = movie;
    }

    public closeVideo(info: VideosUser) {
        this.movie = undefined;
        const videosUser: Array<VideosUser> = JSON.parse(sessionStorage.getItem('videosUser'));
        const foundVideo = videosUser.findIndex((obj) => obj.id === info.id);

        if (foundVideo === -1) {
            videosUser.push(info);
            sessionStorage.setItem('videosUser', JSON.stringify(videosUser));
        } else {
            videosUser[foundVideo].stoppedAt = info.stoppedAt;
            sessionStorage.setItem('videosUser', JSON.stringify(videosUser));
        }
    }

    _handleKeyboardEvent(event: KeyboardEvent) {
        let selectedLine = document.querySelector('.selected');
        const linhas = document.querySelectorAll('.line');
        if (!selectedLine) {
            if (linhas.length > 0) {
                linhas[0].classList.add('selected');
                selectedLine = document.querySelector('.selected');
                this.carousel.reset('teste');
            }
        }

        if (event.key === 'ArrowRight' || event.key === 'ArrowLeft') {
            const screenSize = screen.width;
            const order = parseInt(selectedLine.attributes.getNamedItem('data-order').value, 10);
            let nextOrder = 0;

            if (event.key === 'ArrowRight' && order !== linhas.length - 1 && this.movie === undefined) {
                nextOrder = order + 1;
                const nextLine = document.querySelector('[data-order="' + nextOrder + '"]');
                selectedLine.classList.remove('selected');
                nextLine.classList.add('selected');

                this._swipeCarousel('right', screenSize, order);
            } else if (event.key === 'ArrowLeft' && order !== 0 && this.movie === undefined) {
                nextOrder = order - 1;
                const nextLine = document.querySelector('[data-order="' + nextOrder + '"]');
                selectedLine.classList.remove('selected');
                nextLine.classList.add('selected');

                this._swipeCarousel('left', screenSize, order);
            }
        }

        if (event.key === 'Enter') {
            const linha = document.querySelector('.selected') as HTMLElement;
            const div = linha.children[0] as HTMLElement;
            div.click();
        }
    }

    _handleMouseEvent() {
        const selectedLine = document.querySelector('.selected');
        if (selectedLine) {
            selectedLine.classList.remove('selected');
        }
    }

    private _swipeCarousel(direction, size, order) {
        order = (direction === 'right') ? order + 1 : order;
        const buttonToClick = (direction === 'right') ? '.rightRs' : '.leftRs';
        if (size <= 642) {
            const button = document.querySelector(buttonToClick) as HTMLElement;
            button.click();
        } else  {
            if (order !== 0 && order % 2 === 0) {
                const button = document.querySelector(buttonToClick) as HTMLElement;
                button.click();
            }
        }
    }
}
