import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HomeComponent } from './home/home.component';
import { PagesRoutingModule } from './page-routing.module';
import { PipesModule } from '../pipes/pipes.module';
import { ComponentsModule } from '../components/components.module';
import { MoviesService } from '../services/movies.service';
import { NguCarouselModule } from '@ngu/carousel';

@NgModule({
    imports: [
        HttpModule,
        CommonModule,
        PagesRoutingModule,
        PipesModule,
        ComponentsModule,
        NguCarouselModule
    ],
    declarations: [
        HomeComponent
    ],
    providers: [
        MoviesService
    ]
})

export class PagesModule { }
