import { NgModule } from '@angular/core';
import { CurrencyBR } from './currency.pipe';

@NgModule({
    declarations: [CurrencyBR],
    exports: [CurrencyBR]
})
export class PipesModule {}
