import { Http, Headers, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { ProjectURL } from '../app.config';
import { LoaderService } from './loader.service';

@Injectable()
export class HttpService {

    constructor(
        private http: Http,
        private loaderService: LoaderService) {}

    public get(url: ProjectURL) {
        this.loaderService.display(true);
        const headers = this._setHeader();

        return this.http.get(url, {headers: headers})
            .toPromise()
            .then(response => {
                return this._setResponse(response);
            })
            .catch(response => {
                return this._setResponse(response);
            });
    }

    public post(url: ProjectURL, data: any) {
        this.loaderService.display(true);
        const headers = this._setHeader();

        return this.http.post(url, data, {headers: headers})
            .toPromise()
            .then(response => {
                return this._setResponse(response);
            })
            .catch(response => {
                return this._setResponse(response);
            });
    }

    private _setHeader(): Headers {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers;
    }

    private _setResponse(data): any {
        this.loaderService.display(false);
        return data.json();
    }
}
