import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { setTimeout } from 'core-js/library/web/timers';

@Injectable()
export class LoaderService {
    public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
        false
    );

    display(value: boolean) {
        setTimeout(x => {
            this.status.next(value);
        }, 0);
    }
}
