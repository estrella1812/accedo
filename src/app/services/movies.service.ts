import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpService } from '../services/http.service';
import { Catalog } from '../models/movies';
import { ProjectURL } from '../app.config';

@Injectable()
export class MoviesService {

    constructor(private httpService: HttpService) { }

    public getMovies(): Promise<Catalog> {
        return this.httpService.get(ProjectURL.getMovies);
    }
}
